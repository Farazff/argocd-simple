# Start from the official Golang image
FROM golang:1.21-alpine AS build

WORKDIR /app

COPY go.mod go.sum ./

RUN go mod download

COPY . .

# Build the Go app
RUN go build -o app .

FROM alpine:latest

ENV PORT=8080

COPY --from=build /app/app /app/app

EXPOSE 8080

CMD ["/app/app"]
