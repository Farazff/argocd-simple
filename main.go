package main

import (
	"github.com/labstack/echo/v4"
	"net/http"
)

func main() {
	e := echo.New()
	e.GET("/test/v3", testHandler)
	e.Logger.Fatal(e.Start(":8080"))
}

func testHandler(c echo.Context) error {
	return c.String(http.StatusOK, "Hello, World!")
}
